#! /bin/bash
. para
pid=`ps -ef|grep ${app_name}|grep -v grep`
if [ -n "${pid}" ]
then
    echo "Service is running"
else
    echo "Service not found"
fi