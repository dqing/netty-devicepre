#! /bin/bash
clear
. para
. classpath
nohup java -Dflag=${app_name} -cp ${APPCLASSPATH} ${app_main_class} > /dev/null 2>&1 &

sleep 2
echo "Service has been started"