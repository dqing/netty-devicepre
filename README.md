# netty-http

#### 介绍
基于netty的socket长连接，采用http通信协议，实现http的长连接和短连接代理。
1、作为服务端接收终端长连接请求。
2、作为服务端接收业务端短连接请求。
3、作为长连接客户数请求终端，并接收终端响应处理。
4、作为业务系统的客户端请求业务端数据处理。

#### 软件架构
系统采用springboot+netty开发。
作为一个中间应用：
1、接收业务平台的请求数据，同步阻塞等待，通过socket长连接异步发送设备，轮询等待设备响应，返回业务平台；
2、接收终端设备的请求数据，通过HTTP协议同步转发至业务平台，同步响应。


#### 安装教程

1.  下载源码
2.  mvn 打包(pom实现了一个打包功能)
3.  直接解压打包文件
4.  执行bin目录下启动脚本

#### 使用说明

linux环境部署（建议）
1.  直接解压打包文件
2.  执行bin目录下启动脚本
windows源码运行
1、直接通过idea导入项目
2、运行Application文件


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
