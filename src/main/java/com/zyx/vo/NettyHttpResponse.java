package com.zyx.vo;

import io.netty.handler.codec.http.HttpResponseStatus;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 描述说明:
 *
 * @author DQing
 * @version 1.0
 * @className NettyHttpResponse
 * @date 2020/5/3 17:24
 */
@Data
@AllArgsConstructor
public class NettyHttpResponse {

    /**
     * 响应码，比如200
     */
    private HttpResponseStatus httpResponseStatus;

    /**
     * 消息体,utf-8解码
     */
    private String body;



    public static NettyHttpResponse successResponse(String body) {
        return new NettyHttpResponse(HttpResponseStatus.OK, body);
    }
}
