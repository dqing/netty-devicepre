package com.zyx.vo;

import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.util.concurrent.Promise;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 描述说明:
 *
 * @author DQing
 * @version 1.0
 * @className NettyHttpRequest
 * @date 2020/5/3 17:28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NettyHttpRequest {

    /**
     * 当前的http请求
     */
    FullHttpRequest fullHttpRequest;

    /**
     * 主线程发送完请求后，在该promise上等待
     */
    Promise<NettyHttpResponse> defaultPromise;
}
