package com.zyx.netty.handler;

import com.zyx.service.RequestMsgService;
import com.zyx.service.RequestMsgServiceResolver;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.util.CharsetUtil;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.zyx.constants.Constants.MPSP_URI;

/**
 * 接收业务平台的http请求
 *
 * @author DQing
 * @version 1.0
 * @className BusiRequestHandler
 * @date 2020/3/25 18:09
 */
@Slf4j
@ChannelHandler.Sharable
@Component
public class BusiRequestHandler extends SimpleChannelInboundHandler<FullHttpRequest> {

    @Autowired
    private RequestMsgServiceResolver requestMsgServiceResolver;

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest msg) throws Exception {
        String uri = msg.uri();
        MDC.put(MPSP_URI,uri);
        String content = msg.content().toString(CharsetUtil.UTF_8);
        log.info("收到请求报文，请求URI={}, 请求报文={}",uri,content);
        RequestMsgService requestMsgService = requestMsgServiceResolver.getRequestMsgService(uri);
        requestMsgService.parseMsg(ctx,content);
    }

}
