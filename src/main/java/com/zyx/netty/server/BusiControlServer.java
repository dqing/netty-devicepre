package com.zyx.netty.server;

import io.netty.channel.ChannelFuture;
import io.netty.channel.nio.NioEventLoopGroup;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * 描述说明:
 *
 * @author DQing
 * @version 1.0
 * @className BusiControlServcer
 * @date 2020/5/1 22:32
 */
@Slf4j
@Component
public class BusiControlServer extends AbstractControlServer{

    @Autowired
    @Qualifier("businessBossGroup")
    private NioEventLoopGroup bossGroup;

    @Autowired
    @Qualifier("businessWorkerGroup")
    private NioEventLoopGroup workerGroup;

    @Autowired
    BusiChannelInitializer busiChannelInitializer;

    /**
     * 启动针对业务平台的服务，接收下行请求
     * @param port
     * @return
     * @throws Exception
     */
    public ChannelFuture start(int port) throws Exception {
        ChannelFuture f = super.startServer(port,bossGroup,workerGroup,busiChannelInitializer);
        return f;
    }

    /**
     * 停止服务
     */
    public void destroy() {
       super.destroyServer(bossGroup,workerGroup);
    }
}
