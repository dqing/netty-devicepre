package com.zyx.netty.server;

import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * 描述说明:
 *
 * @author DQing
 * @version 1.0
 * @className ControlServer
 * @date 2020/3/24 11:12
 */
@Slf4j
@Component
public class DeviceControlServer extends AbstractControlServer{

    @Autowired
    @Qualifier("bossGroup")
    private NioEventLoopGroup bossGroup;

    @Autowired
    @Qualifier("workerGroup")
    private NioEventLoopGroup workerGroup;

    @Autowired
    DeviceChannelInitializer deviceChannelInitializer;

    /**
     * 启动针对业务平台的服务，接收下行请求
     * @param port
     * @return
     * @throws Exception
     */
    public ChannelFuture start(int port) throws Exception {
        ChannelFuture f = super.startServer(port,bossGroup,workerGroup,deviceChannelInitializer);
        return f;
    }

    /**
     * 停止服务
     */
    public void destroy() {
        super.destroyServer(bossGroup,workerGroup);
    }

}
