package com.zyx.netty.server;

import com.zyx.netty.handler.BusiRequestHandler;
import com.zyx.netty.handler.DeviceRequestHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 描述说明:
 *
 * @author DQing
 * @version 1.0
 * @className BusiChannelInitializer
 * @date 2020/5/1 22:34
 */
@Slf4j
@Component
public class BusiChannelInitializer extends ChannelInitializer<SocketChannel> {

    @Value("${netty.business.read-timeout}")
    private int readTimeOut;
    @Autowired
    BusiRequestHandler busiRequestHandler;

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast(new LoggingHandler());
        pipeline.addLast( new IdleStateHandler(readTimeOut, 0, 0, TimeUnit.MINUTES));
        pipeline.addLast(new HttpServerCodec());
        pipeline.addLast(new HttpObjectAggregator(1024*1024*1024));
        pipeline.addLast(busiRequestHandler);
    }
}
