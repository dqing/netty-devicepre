package com.zyx.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

/**
 * @Description：动态配置文件获取
 * <p>创建日期：2019-06-26 </p>
 * @version V1.0  
 * @author junjie.cao
 * @see
 */
@Component
public class DynProperties {

    @Autowired
    MessageSource myMessageSource;

    public String get(String key){
        return  get(key,null);
    }
    public String get(String key,String defaultValue){
        return  myMessageSource.getMessage(key,null,defaultValue,null);
    }
}