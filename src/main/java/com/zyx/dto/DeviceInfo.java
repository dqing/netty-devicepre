package com.zyx.dto;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 描述说明:
 *
 * @author DQing
 * @version 1.0
 * @className DeviceInfo
 * @date 2020/5/5 15:05
 */
@Data
@Slf4j
public class DeviceInfo {

    private String deviceCode;
    private String deviceModel;
    private String deviceConfig;
    private String address;
}
