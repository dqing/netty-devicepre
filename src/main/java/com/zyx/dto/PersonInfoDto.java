package com.zyx.dto;

import lombok.Data;

import java.util.List;

/**
 * 请求设备信息
 *
 * @author DQing
 * @version 1.0
 * @className PersonInfoDto
 * @date 2020/4/24 17:08
 */
@Data
public class PersonInfoDto {
    private Long Num;
    private List<PersonInfo> PersonList;
}
