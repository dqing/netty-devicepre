package com.zyx.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

/**
 * 描述说明: 访客用户信息
 *
 * @author DQing
 * @version 1.0
 * @className PersonInfo
 * @date 2020/4/24 11:36
 */
@Data
public class PersonInfo implements Serializable {

    private String personId;
    private String personName;
}
