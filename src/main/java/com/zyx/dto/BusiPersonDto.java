package com.zyx.dto;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * 业务平台请求网关的实体类
 *
 * @author DQing
 * @version 1.0
 * @className BusiPersonDto
 * @date 2020/5/5 22:28
 */
@Slf4j
@Data
public class BusiPersonDto implements Serializable{
    private String rpid;
    private String deviceNo;
    private String userId;
    private String userName;
    private String time;
}
