package com.zyx.dto;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 设备响应报文
 *
 * @author DQing
 * @version 1.0
 * @className ResponseDto
 * @date 2020/5/5 14:53
 */
@Data
@Slf4j
public class ResponseDto {
    protected String responseURL;
    protected Integer code;
    protected String message;
    protected Object data;
}
