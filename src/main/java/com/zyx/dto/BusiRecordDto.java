package com.zyx.dto;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 推送记录 网关请求业务平台的实体类
 *
 * @author DQing
 * @version 1.0
 * @className BusiRecordDto
 * @date 2020/4/27 16:38
 */
@Slf4j
@Data
public class BusiRecordDto {

    private String rpid;
    private String deviceNo;
    private List<PersonInfo> personInfoList;

}
