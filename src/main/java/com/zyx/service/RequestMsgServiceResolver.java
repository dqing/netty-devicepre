package com.zyx.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 描述说明:
 *
 * @author DQing
 * @version 1.0
 * @className RequestMsgServiceResolver
 * @date 2020/3/31 16:41
 */
@Slf4j
@Component
public class RequestMsgServiceResolver implements InitializingBean,ApplicationContextAware {

    private final Map<String,RequestMsgService> handleMap = new ConcurrentHashMap<>();
    private ApplicationContext applicationContext;

    @Override
    public void afterPropertiesSet() throws Exception {
        Map<String, RequestMsgService> beanMap = applicationContext.getBeansOfType(RequestMsgService.class);
        for(String key:beanMap.keySet()){
            this.handleMap.put(beanMap.get(key).msgType(),beanMap.get(key));
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }


    /**
     * 返回message的具体处理类
     * @param msgType
     * @return
     */
    public RequestMsgService getRequestMsgService(String msgType){
        return handleMap.get(msgType);
    }
}
