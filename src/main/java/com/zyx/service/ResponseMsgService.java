package com.zyx.service;

import io.netty.channel.ChannelHandlerContext;

import java.util.Map;

/**
 * 描述说明:
 *
 * @author DQing
 * @version 1.0
 * @className ResponseMsgService
 * @date 2020/3/31 17:45
 */
public interface ResponseMsgService {

    String msgType();

    void parseMsg(ChannelHandlerContext ctx, Map<String, Object> jsonMap);

}
