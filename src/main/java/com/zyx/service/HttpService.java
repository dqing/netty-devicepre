package com.zyx.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author DQing
 * @version 1.0
 * @className HttpServiceImpl
 * @date 2020/4/1 15:44
 */
@Slf4j
@Service
public class HttpService {
    @Autowired
    private CloseableHttpClient httpClient;

    @Autowired
    private RequestConfig config;

    public String doGet(String url) {
        HttpGet httpGet = new HttpGet(url);
        httpGet.setConfig(config);
        try{
            CloseableHttpResponse response = this.httpClient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == 200) {
                return EntityUtils.toString(response.getEntity(), "UTF-8");
            }
        }catch (Exception e){
            log.error("请求远程地址失败,错误原因：{}",e.getMessage());
        }
        return null;
    }

    public String doGet(String url, Map<String, Object> map) {
        try{
            URIBuilder uriBuilder = new URIBuilder(url);
            if (map != null) {
                for (Map.Entry<String, Object> entry : map.entrySet()) {
                    uriBuilder.setParameter(entry.getKey(), entry.getValue().toString());
                }
            }
            return this.doGet(uriBuilder.build().toString());
        }catch (Exception e){

        }
        return null;
    }

    public String doPost(String url, String strJson) {
        log.info("请求报文URL={},请求报文内容content={}",url,strJson);
        HttpClient client = HttpClients.createDefault();
        HttpPost post = new HttpPost(url);
        String response = null;
        try {
            StringEntity s = new StringEntity(strJson, Charset.forName("UTF-8"));
            s.setContentType("application/json");
            post.setEntity(s);
            HttpResponse res = client.execute(post);
            if(res.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
                HttpEntity entity = res.getEntity();
                response = EntityUtils.toString(entity);
            }
        } catch (Exception e) {
            log.error("请求远程地址失败,错误原因：{}",e.getMessage());
        }
        return response;
    }

    public String doPost(String url, Map<String, Object> map) {
        try{
            HttpPost httpPost = new HttpPost(url);
            httpPost.setConfig(config);
            if (map != null) {
                List<NameValuePair> list = new ArrayList<NameValuePair>();
                for (Map.Entry<String, Object> entry : map.entrySet()) {
                    list.add(new BasicNameValuePair(entry.getKey(), entry.getValue().toString()));
                }
                UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(list, "UTF-8");
                httpPost.setEntity(urlEncodedFormEntity);
            }
            CloseableHttpResponse response = this.httpClient.execute(httpPost);
            if (response.getStatusLine().getStatusCode() == 200) {
                return EntityUtils.toString(response.getEntity(), "UTF-8");
            }
        }catch (Exception e){
            log.error("POST请求远程地址失败,错误原因：{}",e.getMessage());
        }
        return null;
    }
}
