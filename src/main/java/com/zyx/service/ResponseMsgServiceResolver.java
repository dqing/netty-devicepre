package com.zyx.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 描述说明: 响应报文解析处理类
 *
 * @author DQing
 * @version 1.0
 * @className ResponseMsgServiceResolver
 * @date 2020/3/31 16:41
 */
@Slf4j
@Component
public class ResponseMsgServiceResolver implements InitializingBean,ApplicationContextAware {

    private final Map<String,ResponseMsgService> handleMap = new ConcurrentHashMap<>();
    private ApplicationContext applicationContext;

    @Override
    public void afterPropertiesSet() throws Exception {
        Map<String, ResponseMsgService> beanMap = applicationContext.getBeansOfType(ResponseMsgService.class);
        for(String key:beanMap.keySet()){
            this.handleMap.put(beanMap.get(key).msgType(),beanMap.get(key));
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    /**
     * 返回message的具体处理类
     * @param msgType
     * @return
     */
    public ResponseMsgService getResponseMsgService(String msgType){
        return handleMap.get(msgType);
    }
}
