package com.zyx.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zyx.resource.DynProperties;
import com.zyx.service.HttpService;
import com.zyx.service.RequestMsgService;
import com.zyx.utils.*;
import com.zyx.constants.Constants;
import com.zyx.constants.RetCode;
import com.zyx.dto.*;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

import static com.zyx.constants.Constants.*;

/**
 * 描述说明:
 *
 * @author DQing
 * @version 1.0
 * @className NotificationRequestMsgServiceImpl
 * @date 2020/4/1 14:03
 */
@Slf4j
@Service
public class NotificationRequestMsgServiceImpl implements RequestMsgService {

    @Autowired
    HttpService httpService;
    @Autowired
    DynProperties dynProperties;

    @Override
    public String msgType() {
        return Constants.NOTIFICATION_URI;
    }

    @Override
    public void parseMsg(ChannelHandlerContext ctx, String content) {
        log.info(" 终端的记录推送报文内容解析-start");
        JSONObject jsonObject = JSONObject.parseObject(content);
        String rpid = SnowFlakeHolder.nextStringId();

        String deviceNo = jsonObject.getString(DEVICE_CODE);
        Map<String,Object> reqMap = new HashMap<>();
        reqMap.put(INNER_RPID, rpid);
        reqMap.put(INNER_DEVICENO, deviceNo);
        String strJson = JSON.toJSONString(reqMap);
        log.info("记录推送请求业务平台报文：{}",strJson);
        String url = dynProperties.get("business.url","");
        String response = httpService.doPost(url,strJson);
        if(!StringUtils.isEmpty(response)){
            JSONObject responseObject = JSONObject.parseObject(response);
            log.info(" 推送至数据业务处理,retCode={},retMsg={}",responseObject.getString(INNER_RES_RETCODE),responseObject.getString(INNER_RES_RETMSG));
            //响应报文
            String seq = jsonObject.getString(DEVICE_RECORD_SEQ);
            String retCode = responseObject.getString(INNER_RES_RETCODE);
            String resStr = getRespData(seq,retCode);
            DynamicMsgUtils.responseMsg(ctx.channel(),resStr,true);
        }else{
            log.error("请求业务平台处理数据失败");
        }
        log.info(" 终端的记录推送报文内容解析-end");
    }

    /**
     * 封装响应设备的报文
     * @param recordId
     * @param retCode
     * @return
     */
    private String getRespData(String recordId,String retCode){
        Map<String,Object> dataMap = new HashMap<>();
        dataMap.put(DEVICE_RECORDID,Integer.valueOf(recordId));
        dataMap.put(DEVICE_TIME,DateTimeUtil.formatTimestamp2String(new Date(),DateTimeUtil.DATE_TIME_PATTERN));
        ResponseDto responseDto = new ResponseDto();
        responseDto.setData(dataMap);
        responseDto.setResponseURL(NOTIFICATION_RESPONSE_URI);
        int code = 0;
        if(!RetCode.RETCODE_OK.equals(retCode)){
            code = 1;
        }
        responseDto.setCode(code);
        responseDto.setMessage(dynProperties.get(retCode));
        JSONObject resStr = (JSONObject) JSON.toJSON(responseDto);
        return resStr.toString();
    }

}
