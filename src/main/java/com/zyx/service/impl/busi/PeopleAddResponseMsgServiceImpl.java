package com.zyx.service.impl.busi;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.zyx.constants.ChannelMap;
import com.zyx.constants.Constants;
import com.zyx.dto.BusiPersonDto;
import com.zyx.dto.PersonInfo;
import com.zyx.dto.PersonInfoDto;
import com.zyx.dto.ResponseDto;
import com.zyx.resource.DynProperties;
import com.zyx.service.AbstractMsgService;
import com.zyx.service.ResponseMsgService;
import com.zyx.utils.DynamicMsgUtils;
import com.zyx.vo.NettyHttpRequest;
import com.zyx.vo.NettyHttpResponse;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.concurrent.Promise;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.zyx.constants.Constants.*;
import static com.zyx.constants.NettyConstants.CURRENT_REQ_BOUND_WITH_THE_CHANNEL;
import static com.zyx.constants.RetCode.*;

/**
 * 描述说明:add people lib member from business request
 *
 * @author DQing
 * @version 1.0
 * @className PeopleAddRequestMsgServiceImpl
 * @date 2020/4/2 11:44
 */
@Slf4j
@Service
public class PeopleAddResponseMsgServiceImpl implements ResponseMsgService {

    @Autowired
    DynProperties dynProperties;

    @Override
    public String msgType() {
        return Constants.INNER_PEOPLE_ADD;
    }

    @Override
    public void parseMsg(ChannelHandlerContext ctx, Map<String, Object> jsonMap) {
        String retMsg = parseMessage(jsonMap);
        log.info("人员信息响应：{}，响应业务平台信息：{}",jsonMap.toString(),retMsg);
        NettyHttpResponse nettyHttpResponse = NettyHttpResponse.successResponse(retMsg);
        NettyHttpRequest nettyHttpRequest = ctx.channel().attr(CURRENT_REQ_BOUND_WITH_THE_CHANNEL).get();
        Promise<NettyHttpResponse> promise = nettyHttpRequest.getDefaultPromise();
        promise.setSuccess(nettyHttpResponse);
    }


    private String parseMessage(Map<String, Object> map){
        Map<String,Object> resMap = new HashMap<>();
        resMap.put(INNER_RES_RETCODE,String.format("%04d",map.get("code")));
        resMap.put(INNER_RES_RETMSG,map.get("message"));
        return JSON.toJSONString(resMap);
    }


}
