package com.zyx.service;

import io.netty.channel.ChannelHandlerContext;

/**
 * 描述说明:
 *
 * @author DQing
 * @version 1.0
 * @className RequestMsgService
 * @date 2020/3/31 16:35
 */
public interface RequestMsgService {

    String msgType();

    void parseMsg(ChannelHandlerContext ctx, String content);
}
