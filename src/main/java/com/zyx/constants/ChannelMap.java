package com.zyx.constants;

import io.netty.channel.Channel;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 描述说明:
 *
 * @author DQing
 * @version 1.0
 * @className ChannelMap
 * @date 2020/3/25 13:58
 */
public class ChannelMap {

    public static int channelNum=0;
    /**
     * 存储终端与平台连接的channel key-terminalID value-channel
     */
    public static ConcurrentHashMap<String,Channel> deviceChannelMap;
    /**
     * 存储平台与业务平台连接的channel  key-channelID+option  value-channel
     * channelId为存储终端与平台连接的channelID
     * option：具体的操作类型
     */
    public static ConcurrentHashMap<String,Map<String,Object>> requsetChannelMap;

    static {
        deviceChannelMap = new ConcurrentHashMap<>();
        requsetChannelMap = new ConcurrentHashMap<>();
    }

    /**
     * get channel map between terminal and platform
     * @return
     */
    public static ConcurrentHashMap<String, Channel> getDeviceChannelHashMap() {
        return deviceChannelMap;
    }

    /**
     * get channel map between business and platform
     * @return
     */
    public static ConcurrentHashMap<String, Map<String,Object>> getRequestChannelHashMap() {
        return requsetChannelMap;
    }
    /**
     * 根据terminalID获取channel
     * @param name
     * @return
     */
    public static Channel getDeviceChannelByName(String name){
        if(deviceChannelMap==null||deviceChannelMap.isEmpty()){
            return null;
        }
        return deviceChannelMap.get(name);
    }

    /**
     * 根据channelID+操作 获取请求的channel
     * @param name
     * @return
     */
    public static Map<String,Object> getRequestChannelByName(String name){
        if(requsetChannelMap==null||requsetChannelMap.isEmpty()){
            return null;
        }
        return requsetChannelMap.get(name);
    }

    /**
     * add channel between terminal and platform
     * @param name
     * @param channel
     */
    public static void addDeviceChannel(String name,Channel channel){
        Channel orgChannel = deviceChannelMap.get(name);
        if(StringUtils.isEmpty(orgChannel)){
            deviceChannelMap.put(name,channel);
            channelNum++;
        }
    }

    /**
     * add channel between business and platform
     * @param name
     * @param map
     */
    public static void addRequestChannel(String name,Map<String,Object> map){
        requsetChannelMap.put(name,map);
        channelNum++;
    }

    /**
     * remove channel between terminal and platform
     * @param name
     * @return
     */
    public static int removeDeviceChannelByName(String name){
        if(deviceChannelMap.containsKey(name)){
            deviceChannelMap.remove(name);
            return 0;
        }else{
            return 1;
        }
    }

    /**
     * remove channel between business and platform
     * @param name
     * @return
     */
    public static int removeRequestChannelByName(String name){
        if(requsetChannelMap.containsKey(name)){
            requsetChannelMap.remove(name);
            return 0;
        }else{
            return 1;
        }
    }
}
