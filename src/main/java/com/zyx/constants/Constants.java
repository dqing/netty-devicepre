package com.zyx.constants;

/**
 * 描述说明: 常变量
 *
 * @author DQing
 * @version 1.0
 * @className Constants
 * @date 2020/3/24 15:37
 */
public class Constants {

    public static String MPSP_LOG = "MPSP";
    public static String START_TIME = "startTime";
    public static String MPSP_URI = "URI";
    /**
     * 接口常变量
     */
    public static String DEVICE_RESPONSE_URL = "ResponseURL";
    public static String DEVICE_DATA = "Data";
    public static String DEVICE_ADDRESS = "Address";
    public static String DEVICE_RESPONSE = "Response";
    public static String DEVICE_CODE = "DeviceCode";
    public static String DEVICE_TIME = "Time";
    public static String DEVICE_RECORDID= "RecordID";
    /**
     * 推送记录
     */
    public static String DEVICE_RECORD_SEQ = "Seq";

    /**
     * 内部通信字段
     */
    public static String INNER_RPID = "rpid";
    public static String INNER_REQDATE = "reqDate";
    public static String INNER_REQTIME = "reqTime";
    public static String INNER_DEVICENO = "deviceNo";
    public static String INNER_RES_RETCODE = "retCode";
    public static String INNER_RES_RETMSG = "retMsg";

    /**
     * 平台请求终端URI
     */
    public static String DEVICE_BASIC_INFO_URI = "/LAPI/V1.0/System/DeviceBasicInfo";
    public static String DEVICE_PERSON_URI = "/LAPI/V1.0/PeopleLibraries/3/People";
    /**
     * 终端请求平台URI
     */
    public static String HEART_REPORT_URI = "/LAPI/V1.0/PACS/Controller/HeartReportInfo";
    public static String NOTIFICATION_URI = "/LAPI/V1.0/System/Event/Notification/PersonVerification";
    public static String NOTIFICATION_RESPONSE_URI = "/LAPI/V1.0/PACS/Controller/Event/Notifications";
    /**
     * 编解码标识
     */
    public static String PROTOCOL_RESPONSE = "HTTP";
    public static String PROTOCOL_REQUEST_CONTAINS = "HTTP/1.1";
    public static String REQUEST_HANDLER = "deviceRequest";
    public static String RESPONSE_HANDLER = "deviceResponse";
    public static String DECODER_HANDLER = "decoder";
    public static String REQUEST_HANDLER_ENCODER = "requestEncoder";
    public static String RESPONSE_HANDLER_ENCODER = "responseEncoder";
    public static String REQUEST_HANDLER_DECODER = "requestDecoder";
    public static String RESPONSE_HANDLER_DECODER = "responseDecoder";

    /**
     * 接口通用请求地址
     */
    public static String INNER_PEOPLE_ADD = "/ys/person/add";

    /**
     * 响应报文类型
     */
    public static String HTTP_METHOD_POST = "POST";
    public static String HTTP_METHOD_GET = "GET";

    public static String REDIS_PREFIX = "DEVICE_IP:";
    public static String SEPARATOR = ",";
}
