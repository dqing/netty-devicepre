package com.zyx.constants;

/**
 * 描述说明:
 *
 * @author DQing
 * @version 1.0
 * @className RetCode
 * @date 2020/4/24 17:36
 */
public class RetCode {

    public static String RETCODE_OK = "0000";
    public static String RETCODE_9001 = "9001";
    public static String RETCODE_1000 = "1000";
    public static String RETCODE_1001 = "1001";
    public static String RETCODE_1002 = "1002";
    public static String RETCODE_1003 = "1003";
}
