package com.zyx.constants;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 描述说明:
 *
 * @author DQing
 * @version 1.0
 * @className NamedThreadFactory
 * @date 2020/5/3 17:20
 */
public class NamedThreadFactory implements ThreadFactory{

    private static final Integer INITIAL_VALUE = 1;
    private static final AtomicInteger POOL_NUMBER = new AtomicInteger(INITIAL_VALUE);
    private final ThreadGroup group;
    private final AtomicInteger threadNumber = new AtomicInteger(INITIAL_VALUE);
    private final String namePrefix;

    public NamedThreadFactory(String name) {
        SecurityManager s = System.getSecurityManager();
        group = (s != null) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
        if (null == name || name.isEmpty()) {
            name = "pool";
        }
        namePrefix = name + "-" + POOL_NUMBER.getAndIncrement() + "-thread-";
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(group, r, namePrefix + threadNumber.getAndIncrement(), 0);
        if (t.isDaemon()) {
            t.setDaemon(false);
        }
        if (t.getPriority() != Thread.NORM_PRIORITY) {
            t.setPriority(Thread.NORM_PRIORITY);
        }
        return t;
    }
}
