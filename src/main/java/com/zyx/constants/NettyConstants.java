package com.zyx.constants;

import com.zyx.constants.NamedThreadFactory;
import com.zyx.vo.NettyHttpRequest;
import io.netty.channel.DefaultEventLoop;
import io.netty.util.AttributeKey;

/**
 * 描述说明:
 *
 * @author DQing
 * @version 1.0
 * @className NettyConstants
 * @date 2020/5/3 17:27
 */
public class NettyConstants {

    public static final DefaultEventLoop NETTY_RESPONSE_PROMISE_NOTIFY_EVENT_LOOP =  new DefaultEventLoop(null, new NamedThreadFactory("NettyResponsePromiseNotify"));

    public static final AttributeKey<NettyHttpRequest> CURRENT_REQ_BOUND_WITH_THE_CHANNEL =
            AttributeKey.valueOf("CURRENT_REQ_BOUND_WITH_THE_CHANNEL");
}
