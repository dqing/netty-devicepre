package com.zyx.log;

import com.zyx.constants.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalTime;

import static com.zyx.constants.Constants.MPSP_URI;
import static com.zyx.constants.Constants.START_TIME;

/**
 * 描述说明: 简要日志打印
 *
 * @author DQing
 * @version 1.0
 * @className DataMsgLog
 * @date 2020/1/8 15:05
 */
public class MpspLog {
    private static Logger logger;
    private static Logger logger(){
        if(logger == null){
            logger = LoggerFactory.getLogger(Constants.MPSP_LOG);
        }
        return logger;
    }


    public static synchronized void logWrite(String message, long time){
        StringBuffer sb=new StringBuffer();
        sb.append(StringUtils.trimWhitespace(LocalDate.now().toString())).append(Constants.SEPARATOR)
                .append(StringUtils.trimWhitespace(LocalTime.now().toString())).append(Constants.SEPARATOR);
        String uri = MDC.get(MPSP_URI);
        sb.append(uri).append(Constants.SEPARATOR);
        String startTime = MDC.get(START_TIME);
        sb.append(System.currentTimeMillis()-Long.valueOf(startTime));
        logger().info(sb.toString());
        sb=null;
    }
}
