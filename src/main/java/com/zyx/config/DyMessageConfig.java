package com.zyx.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

/**
 * 描述说明:
 *
 * @author DQing
 * @version 1.0
 * @className DyMessageConfig
 * @date 2020/5/4 22:03
 */
@Configuration
public class DyMessageConfig {

    @Value("${dynamic.file.name}")
    String baseStr;

    @Bean
    public MessageSource myMessageSource() {
        String[] baseNameArr = baseStr.split(",");
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames(baseNameArr);
        //reload dynprops every 60 seconds
        messageSource.setCacheSeconds(60);
        messageSource.setDefaultEncoding("UTF-8");
        messageSource.setUseCodeAsDefaultMessage(true);
        return messageSource;
    }
}
