package com.zyx.config;

import io.netty.channel.nio.NioEventLoopGroup;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 描述说明:
 *
 * @author DQing
 * @version 1.0
 * @className EventLoopGroupConfig
 * @date 2020/3/24 11:21
 */
@Configuration
public class EventLoopGroupConfig {

    @Value("${netty.device.threads.boss}")
    private int bossThreadsNum;

    @Value("${netty.device.threads.worker}")
    private int workerThreadsNum;

    @Value("${netty.business.threads.boss}")
    private int businessBossThreadsNum;

    @Value("${netty.business.threads.worker}")
    private int businessWorkerThreadsNum;

    /**
     * 负责TCP连接建立操作 绝对不能阻塞
     * @return
     */
    @Bean(name = "bossGroup")
    public NioEventLoopGroup bossGroup() {
        return new NioEventLoopGroup(bossThreadsNum);
    }

    /**
     * 负责Socket读写操作 绝对不能阻塞
     * @return
     */
    @Bean(name = "workerGroup")
    public NioEventLoopGroup workerGroup() {
        return new NioEventLoopGroup(workerThreadsNum);
    }

    /**
     * Handler中出现IO操作(如数据库操作，网络操作)使用这个
     * @return
     */
    @Bean(name = "businessBossGroup")
    public NioEventLoopGroup businessBossGroup() {
        return new NioEventLoopGroup(businessBossThreadsNum);
    }

    /**
     * Handler中出现IO操作(如数据库操作，网络操作)使用这个
     * @return
     */
    @Bean(name = "businessWorkerGroup")
    public NioEventLoopGroup businessWorkerGroup() {
        return new NioEventLoopGroup(businessWorkerThreadsNum);
    }



}
